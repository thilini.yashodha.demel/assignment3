#include <stdio.h>
int main()
{
    char C;
    int lowercase_vowel , uppercase_vowel;
    printf ("Enter an alphabet letter:  ");
    scanf ("%c" , &C);
    lowercase_vowel=(C=='a'||C=='e'||C=='i'||C=='o'||C=='u');
    uppercase_vowel=(C=='A'||C=='E'||C=='I'||C=='O'||C=='U');
    if (lowercase_vowel || uppercase_vowel)
    printf("%c is a vowel." , C);
    else
    printf ("%c is a consonant");
    return 0;
}
